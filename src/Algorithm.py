import time
# from Board import update_callback

def solve_n_queens(n, update_callback):
    def is_safe(board, row, col):
        for i in range(row):
            if board[i][col] == 1:
                return False

        for i, j in zip(range(row, -1, -1), range(col, -1, -1)):
            if board[i][j] == 1:
                return False

        for i, j in zip(range(row, -1, -1), range(col, n)):
            if board[i][j] == 1:
                return False

        return True

    def solve(board, row):
        if row >= n:
            return True
        for col in range(n):
            if is_safe(board, row, col):
                board[row][col] = 1
                update_callback(board)
                time.sleep(0.5) 
                if solve(board, row + 1):
                    return True
                board[row][col] = 0
                update_callback(board)
                time.sleep(0.5) 
        return False

    board = [[0 for _ in range(n)] for _ in range(n)]
    if solve(board, 0):
        return board
    else:
        return None
