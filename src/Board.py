import pygame

def draw_board(screen, board, width, height):
    n = len(board)
    square_size = 100
    colors = [pygame.Color("white"), pygame.Color("gray")]

    for row in range(n):
        for col in range(n):
            color = colors[(row + col) % 2]
            pygame.draw.rect(screen, color, pygame.Rect(col * square_size, row * square_size, square_size, square_size))
            if board[row][col] == 1:
                center = (col * square_size + square_size // 2, row * square_size + square_size // 2)
                pygame.draw.circle(screen, pygame.Color("red"), center, square_size // 4)

def update_visualization(screen, board, width, height):
    draw_board(screen, board, width, height)
    pygame.display.flip()
