import pygame
import sys
from Algorithm import solve_n_queens
from Board import update_visualization, draw_board
from ui import InputBox, Button

# Constants


def main():
    pygame.init()
    screen = pygame.display.set_mode((500,500))
    pygame.display.set_caption("N-Queens Visualizer")

    font = pygame.font.Font(None, 45)
    input_box = InputBox(200, 250, 100, 40, font)
    button = Button(200, 300, 100, 40, "Enter", font)

    clock = pygame.time.Clock()
    done = False
    n = 0
    solving = False

    def update_callback(board):
        update_visualization(screen, board, SCREEN_WIDTH, SCREEN_HEIGHT)

    while not done:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                done = True
            if not solving:
                input_box.handle_event(event)
                if button.handle_event(event):
                    try:
                        n = int(input_box.text)
                        screen.fill((30, 30, 30))
                        board = [[0 for _ in range(n)] for _ in range(n)]
                        SCREEN_WIDTH, SCREEN_HEIGHT = n*100, n*100
                        screen = pygame.display.set_mode((SCREEN_WIDTH,SCREEN_HEIGHT))
                        pygame.display.flip()
                        solving = True
                        if solve_n_queens(n, update_callback):
                            draw_board(screen, board, SCREEN_WIDTH, SCREEN_HEIGHT)
                            print(f"Solution for N = {n}")
                        else:
                            print(f"No solution exists for N = {n}")
                        solving = False
                    except ValueError:
                        print("Please enter a valid number.")

        if not solving:
            screen = pygame.display.set_mode((500,500))
            screen.fill((30, 30, 30))
            input_box.draw(screen)
            button.draw(screen)
            pygame.display.flip()

        clock.tick(30)

    pygame.quit()
    sys.exit()


if __name__ == "__main__":
    main()
